execute pathogen#infect()

"Ale
"//let g:ale_linters = {
"//\   'javascript': ['standard'],
"//\}
"//let g:ale_fixers = {'javascript': ['standard']}

"Formatting
set encoding=utf-8
syntax on
filetype plugin indent on
set tabstop=2
set shiftwidth=2
set expandtab
set showcmd

"Directory
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_list_hide= '.*\.sw.$,\.DS_Store'

"Window
set splitbelow
set splitright

"Controls
"inoremap jk <esc>
inoremap jj <esc>

"Search
set ignorecase
set smartcase
"set hlsearch

set clipboard=unnamed

let mapleader = " "

"Hardmode
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

"Theme
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='base16'
set colorcolumn=112
colorscheme eva
highlight ColorColumn ctermbg=darkgray
set number
set nuw=4
set linespace=3
set guifont=Fira\ Code:h12
set cursorline
highlight LineNr ctermfg=9
