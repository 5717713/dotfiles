#history -c

export PATH="$PATH:/opt/yarn-[version]/bin"
export PATH="$PATH:$HOME/.rvm/bin"
export PATH="$HOME/.cargo/bin:$PATH"
alias python="/usr/local/bin/python3"

#Prompt string
export PS1="\[\e[45m\][\t]\[\e[0m\] \[\e[1m\]⚖︎\[\e[0m/\]\W " 

#Escape reset/clear scrollback
alias reset="clear && printf '\e[3J' && history -c"

#😌
stoptime() {
  diary="$HOME/desktop/diary.txt"

  startDate=$(date +"%d-%m-%Y")
  startTime=$(date +"%T")

  echo "$startTime"

	sleep $1
  setsid mplayer ~/495926__jackamadon__beeping-danger.wav & >/dev/null 
  pid=$!
	sleep 3
  kill -2 $pid
  
  endDate=$(date +"%d-%m-%Y")
  endTime=$(date +"%T")

  lastDate=$(grep -E '^[0-9]{2}-[0-9]{2}-[0-9]{4}' ${diary} |  tail -1 )
  
  #Check if entry writeable
  if [ ! -z "$2" ]
    then

      if [[ "$lastDate" != "$startDate" ]]
        then
          echo "$endDate" >> $diary
      fi

      if [[ "$startDate" != "$endDate" ]]
        then
          echo "$endDate" >> $diary
          echo "$startTime - $endTime $2" >> $diary
      fi 

      echo "$startTime - $endTime $2" >> $diary
  fi
}

#Homebrew
alias brewery="brew update && brew upgrade && brew cleanup"

#Ngrok
uplink() {
	ngrok http ${1:-10001} --region eu
}

#Files
spot(){
	# $1 command any item matching $2 query in file name
	echo "$1"
	find . -name "*$2*"
}

mkcdir() {
  mkdir -p -- "$1" && cd -P -- "$1"
}

tree() {
	find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'
}

recent() {
	find "$1" -mtime +5
}

#Dl + mp3
alias pullmp3="youtube-dl --extract-audio --audio-format mp3 -o '%(title)s.%(ext)s'"

#Purge id3v2
alias purgetags="id3v2 --delete-all"

#Purge exif
purgexif() {
	mogrify -strip "$1"
}

#Toggle hidden files
alias showhidden="defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app"
alias hidehidden="defaults write com.apple.finder AppleShowAllFiles FALSE; killall Finder /System/Library/CoreServices/Finder.app"

#Toggle creative cloud autorun
alias ccautorunoff="launchctl unload -w /Library/LaunchAgents/com.adobe.AdobeCreativeCloud.plist"
alias ccautorunon="launchctl load -w /Library/LaunchAgents/com.adobe.AdobeCreativeCloud.plist"

